const undefinedCheck = require("./modules/undefinedCheck");

function calculateMatchesWonPerTeamPerYear(inputTotalMatches){

    if(undefinedCheck(inputTotalMatches)){
        return {};
    }

    const output = {};
    for(let match of inputTotalMatches){
        let year=match.season;
        let winnerTeam=match.winner;
        
        if(!output[year]){
            output[year]={};
        }

        if(!output[year][winnerTeam]){
            output[year][winnerTeam]=1;
        }
        else{
            output[year][winnerTeam]+=1;
        }
    }

    return output;
}

module.exports=calculateMatchesWonPerTeamPerYear;