const getAllBatsmanStatsForYear = require("./modules/getAllBatsmanStatsForYear");
const getTotalSeasons = require("./modules/getTotalSeasons");
const undefinedCheck = require("./modules/undefinedCheck");

function calculateBatsmanStrikeRatePerSeason(inputMatches, inputDeliveries, inputBatsman) {

    if (undefinedCheck(inputMatches, inputDeliveries, inputBatsman)) {
        return {};
    }
    
    const output = {};
    const seasons = getTotalSeasons(inputMatches);
    seasons.sort();

    for (let year of seasons) {
        const stats = getAllBatsmanStatsForYear(inputDeliveries, inputMatches, parseInt(year));
        output[year] = (stats[inputBatsman].Runs) / (stats[inputBatsman].Balls);
    }

    return output;
}

module.exports = calculateBatsmanStrikeRatePerSeason;