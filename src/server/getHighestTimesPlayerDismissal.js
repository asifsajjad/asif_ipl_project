const getBatsmanWicketTakers = require("./modules/getBatsmanWicketTakers");
const undefinedCheck = require("./modules/undefinedCheck");

function getHighestTimesPlayerDismissal(inputDeliveries) {

    if (undefinedCheck(inputDeliveries)) {
        return {};
    }

    const output = {};
    let requiredBatsman = '', requiredWicket = 0, requiredBowler = '';
    const dismissalData = getBatsmanWicketTakers(inputDeliveries);
    for (let [batsmanName, data] of Object.entries(dismissalData)) {
        for (let [bowlerName, wicket] of Object.entries(data)) {
            if (wicket > requiredWicket) {
                requiredWicket = wicket;
                requiredBatsman = batsmanName;
                requiredBowler = bowlerName;
            }
        }
    }
    output['Batsman'] = requiredBatsman;
    output['Bowler'] = requiredBowler;
    output['Wickets'] = requiredWicket;
    return output;

}

module.exports = getHighestTimesPlayerDismissal;