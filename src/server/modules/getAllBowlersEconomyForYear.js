const getAllBowlersStatsForYear = require("./getAllBowlersStatsForYear");

function getAllBowlersEconomyForYear(inputMatches,inputDeliveries,inputYear){
    const output={};
    const bowlersStat= getAllBowlersStatsForYear(inputDeliveries,inputMatches,inputYear);
    for(const [bowler,details] of Object.entries(bowlersStat)){
        if (!output[bowler]) {
            output[bowler] = 0;
        }


        output[bowler] += (details.Runs)*6/(details.Balls);
    }

    return output;
}

module.exports=getAllBowlersEconomyForYear;