function getBatsmanWicketTakers(inputDeliveries){

    const output={};
    for(ballReport of inputDeliveries){
        if(ballReport.player_dismissed){
            let batsman=ballReport.player_dismissed;
            if(!output[batsman]){
                output[batsman]={};
            }
            let bowler= ballReport.bowler;
            if(!output[batsman][bowler]){
                output[batsman][bowler]=1;
            }
            else{
                output[batsman][bowler] += 1;
            }
        }
    }
    return output;

}

module.exports=getBatsmanWicketTakers;