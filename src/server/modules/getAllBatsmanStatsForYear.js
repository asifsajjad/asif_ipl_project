const getBatsmanStatPerMatch = require("./getBatsmanStatsPerMatch");
const getMatchIdOfYear = require("./getMatchIdOfYear");

function getAllBatsmanStatsForYear(inputDeliveries, inputMatches, inputYear){
    const output={};
    const matchesId= getMatchIdOfYear(inputMatches,inputYear);
    for(let id of matchesId){
        const batsmenStat= getBatsmanStatPerMatch(inputDeliveries, id);
        for(const [batsman,details] of Object.entries(batsmenStat)){
            if (!output[batsman]) {
                output[batsman] = {};
            }

            if(!output[batsman]['Runs']){
                output[batsman]['Runs']=0;
            }

            if(!output[batsman]['Balls']){
                output[batsman]['Balls'] = 0;
            }

            output[batsman]['Runs'] += details.Runs;
            output[batsman]['Balls'] += details.Balls;
        }

    }

    return output;
}

module.exports=getAllBatsmanStatsForYear;