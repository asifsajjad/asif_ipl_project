function getMatchIdOfYear(inputTotalMatches, inputYear){
    const output=[];
    for(let match of inputTotalMatches){
        let season = parseInt(match.season);
        let id = parseInt(match.id);
        if(season===inputYear){
            output.push(id);
        }
    }
    return output;
}

module.exports=getMatchIdOfYear;