function getExtrasPerMatchId(inputTotalDeliveries, inputMatchId) {
    const output = {};
    for (let ballReport of inputTotalDeliveries) {
        let id = parseInt(ballReport.match_id);
        
        if (inputMatchId === id) {
            let bowlingTeam = ballReport.bowling_team;
            let extraRuns = parseInt(ballReport.extra_runs);
            
            if (!output[bowlingTeam]) {
                output[bowlingTeam] = 0;
            }

            output[bowlingTeam] += extraRuns;
        }
    }

    return output;
}

module.exports = getExtrasPerMatchId;