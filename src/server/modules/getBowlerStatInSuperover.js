function getBowlerStatInSuperover(inputDeliveries){

    const output= {};
    for(let ballReport of inputDeliveries){

        if (ballReport.is_super_over==1) {
            let bowler = ballReport.bowler;
            let totalRuns = parseInt(ballReport.total_runs);
            let penaltyRuns= parseInt(ballReport.penalty_runs);
            let byeRuns= parseInt(ballReport.bye_runs);
            let legbyeRuns= parseInt(ballReport.legbye_runs);
            let runsConsidered = totalRuns - penaltyRuns - byeRuns - legbyeRuns;
            

            
            if (!output[bowler]) {
                output[bowler] = {};
            }

            if(!output[bowler]['Runs']){
                output[bowler]['Runs']=0;
            }

            if(!output[bowler]['Balls']){
                output[bowler]['Balls'] = 0;
            }

            output[bowler]['Runs'] += runsConsidered;
            if(ballReport.wide_runs==0 && ballReport.noball_runs==0){
                output[bowler]['Balls']+=1;
            }
            
        }
    }

    return output;
}

module.exports=getBowlerStatInSuperover;