const csvjson = require('csvjson');
const fs = require('fs');

function convertCsvToJson(csvFilePath) {

    const data = fs.readFileSync(csvFilePath, { encoding: 'utf8' });

    const options = {
        delimiter: ',', // optional
        quote: '"' // optional
    };

    const jsonObj = csvjson.toObject(data, options);

    return jsonObj;
}

module.exports=convertCsvToJson;