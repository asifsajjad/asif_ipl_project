function undefinedCheck(...userArg){
    const argArr= [...userArg];
    for(let item of argArr){
        if(item === undefined)
        {
            return true;
        }
    }
    return false;
}

module.exports=undefinedCheck;