function getBatsmanStatPerMatch(inputDeliveries,inputMatchId){
    const output= {};
    for(let ballReport of inputDeliveries){
        let id = parseInt(ballReport.match_id);

        if (inputMatchId === id) {
            let batsman = ballReport.batsman;
            let batsmanRuns = parseInt(ballReport.batsman_runs);
                        
            if (!output[batsman]) {
                output[batsman] = {};
            }

            if(!output[batsman]['Runs']){
                output[batsman]['Runs']=0;
            }

            if(!output[batsman]['Balls']){
                output[batsman]['Balls'] = 0;
            }

            output[batsman]['Runs'] += batsmanRuns;
            if(ballReport.wide_runs==0){
                output[batsman]['Balls']+=1;
            }
            
        }
    }

    return output;
}

module.exports=getBatsmanStatPerMatch;