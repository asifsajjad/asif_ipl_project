const undefinedCheck = require("./undefinedCheck");

function getManOfTheMatchCount(inputMatches) {

    if (undefinedCheck(inputMatches)) {
        return {};
    }

    const output = {};
    for (let match of inputMatches) {
        if (!output[match.season]) {
            output[match.season] = {};
        }

        if (!output[match.season][match.player_of_match]) {
            output[match.season][match.player_of_match] = 1;
        }
        else {
            output[match.season][match.player_of_match] += 1;
        }

    }

    return output;
}

module.exports=getManOfTheMatchCount;