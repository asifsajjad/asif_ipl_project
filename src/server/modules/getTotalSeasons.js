function getTotalSeasons(inputMatches){
    const output=[];
    for(let match of inputMatches){
        if(!output.includes(match.season)){
            output.push(match.season);
        }
    }

    return output;

}

module.exports=getTotalSeasons;