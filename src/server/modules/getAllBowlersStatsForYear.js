const getBowlersStatPerMatch = require("./getBowlersStatsPerMatch");
const getMatchIdOfYear = require("./getMatchIdOfYear");

function getAllBowlersStatsForYear(inputDeliveries, inputMatches, inputYear){
    const output={};
    const matchesId= getMatchIdOfYear(inputMatches,inputYear);
    for(let id of matchesId){
        const bowlersStat= getBowlersStatPerMatch(inputDeliveries, id);
        for(const [bowler,details] of Object.entries(bowlersStat)){
            if (!output[bowler]) {
                output[bowler] = {};
            }

            if(!output[bowler]['Runs']){
                output[bowler]['Runs']=0;
            }

            if(!output[bowler]['Balls']){
                output[bowler]['Balls'] = 0;
            }

            output[bowler]['Runs'] += details.Runs;
            output[bowler]['Balls'] += details.Balls;
        }

    }

    return output;
}

module.exports=getAllBowlersStatsForYear;