const getExtrasPerMatchId = require("./modules/getExtrasPerMatchId");
const getMatchIdOfYear = require("./modules/getMatchIdOfYear");
const undefinedCheck = require("./modules/undefinedCheck");

function teamExtrasForYear(inputMatches, inputDeliveries, inputYear) {

    if(undefinedCheck(inputMatches,inputDeliveries,inputYear)){
        return {};
    }

    const matchIds = getMatchIdOfYear(inputMatches, inputYear);
    const output = {};
    for (let id of matchIds) {
        const matchExtras = getExtrasPerMatchId(inputDeliveries, id);
        for (const [team, runs] of Object.entries(matchExtras)) {
            if (!output[team]) {
                output[team] = 0;
            }

            output[team] += runs;
        }

    }
    return output;
}

module.exports= teamExtrasForYear;