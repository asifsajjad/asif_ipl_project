const getAllBowlersEconomyForYear = require("./modules/getAllBowlersEconomyForYear");
const undefinedCheck = require("./modules/undefinedCheck");

function getTopTenEconomicalBowlerForYear(inputMatches, inputDeliveries, inputYear) {

    if(undefinedCheck(inputMatches,inputDeliveries,inputYear)){
        return [];
    }

    const output = [];
    let bowlersStat = getAllBowlersEconomyForYear(inputMatches, inputDeliveries, inputYear);
    let outputArr = Object.entries(bowlersStat);
    outputArr.sort((a, b) => {
        return a[1] - b[1];
    })
    for (let index = 0; index < 10; index++) {
        if (index >= outputArr.length) {
            break;
        }
        output.push(outputArr[index]);
    }
    return output;


}

module.exports = getTopTenEconomicalBowlerForYear;