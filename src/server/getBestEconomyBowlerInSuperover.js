const getBowlerStatInSuperover = require("./modules/getBowlerStatInSuperover");
const undefinedCheck = require("./modules/undefinedCheck");

function getBestEconomyBowlerInSuperover(inputDeliveries) {

    if (undefinedCheck(inputDeliveries)) {
        return {};
    }

    const output = {};
    const totalBowlerStat = getBowlerStatInSuperover(inputDeliveries);
    let requiredEconomy = 100, requiredName = '';
    for (let [name, data] of Object.entries(totalBowlerStat)) {
        economy = parseInt(data.Runs) * 6 / parseInt(data.Balls);
        if (economy < requiredEconomy) {
            requiredEconomy = economy;
            requiredName = name;
        }
    }
    output['Bowler'] = requiredName;
    output['Economy'] = requiredEconomy;
    return output;
}

module.exports = getBestEconomyBowlerInSuperover