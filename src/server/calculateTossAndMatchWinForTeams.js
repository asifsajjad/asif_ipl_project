const undefinedCheck = require("./modules/undefinedCheck");

function calculateTossAndMatchWinForTeams(inputMatches) {

    if (undefinedCheck(inputMatches)) {
        return {};
    }

    const output = {};

    for (let match of inputMatches) {
        
        if (match.toss_winner === match.winner) {

            if (!output[match.winner]) {
                output[match.winner] = 1;
            }
            else {
                output[match.winner] += 1;
            }
        }
    }

    return output;
}

module.exports = calculateTossAndMatchWinForTeams;