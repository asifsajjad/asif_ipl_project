const getManOfTheMatchCount = require("./modules/getManOfTheMatchCount");
const undefinedCheck = require("./modules/undefinedCheck");

function getHighestManOfTheMatchPlayerPerSeason(inputMatches){

    if(undefinedCheck(inputMatches)){
        return {};
    }
    
    const motmPlayers= getManOfTheMatchCount(inputMatches);
    const output={};
    for(let [seasonName, seasonData] of Object.entries(motmPlayers)){
        if(!output[seasonName]){
            output[seasonName]= {};
        }
        let desiredPlayer='';
        let desiredPlayerCount=0;
        for(let [name, values] of Object.entries(seasonData)){
            if(values>desiredPlayerCount){
                desiredPlayerCount=values;
                desiredPlayer=name;
            }
        }
        output[seasonName][desiredPlayer]=desiredPlayerCount;
    }

    return output;
}

module.exports=getHighestManOfTheMatchPlayerPerSeason;