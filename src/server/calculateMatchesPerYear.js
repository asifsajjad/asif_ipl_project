const undefinedCheck = require("./modules/undefinedCheck");

function calculateMatchesPerYear(inputTotalMatches){

    if(undefinedCheck(inputTotalMatches)){
        return {};
    }

    const output ={};
    for(let match of inputTotalMatches){
        if(!output[match.season]){
            output[match.season]=1;
        }
        else{
            output[match.season]+=1;
        }
    }
    return output;
}

module.exports=calculateMatchesPerYear;