const calculateMatchesPerYear = require('./calculateMatchesPerYear');
const calculateMatchesWonPerTeamPerYear = require('./calculateMatchesWonPerTeamPerYear');
const convertCsvToJson = require('./modules/convertCsvToJson');
const calculateTeamExtrasForYear = require('./calculateTeamExtrasForYear');
const getTopTenEconomicalBowlerForYear = require('./getTopTenEconomicalBowlerForYear');
const saveToFile = require('./modules/saveToFile');
const calculateTossAndMatchWinForTeams= require('./calculateTossAndMatchWinForTeams');
const getHighestManOfTheMatchPlayerPerSeason= require('./getHighestManOfTheMatchPlayerPerSeason');
const calculateBatsmanStrikeRatePerSeason= require('./calculateBatsmanStrikeRatePerSeason');
const getHighestTimesPlayerDismissal = require('./getHighestTimesPlayerDismissal');
const getBestEconomyBowlerInSuperover = require('./getBestEconomyBowlerInSuperover');

// const inputMatches = '../data/matches.csv';
// const inputDeliveries = '../data/deliveries.csv';
const outMatchesPerYear = '../public/output/matchesPerYear.json';
const outMatchesWonByTeams = '../public/output/matchesWonByTeamsPerYear.json';
const outTeamExtras = '../public/output/teamExtrasForYear.json';
const outTopBowlers = '../public/output/topTenEconomicalBowlersForYear.json';
const outTossAndMatchWin= '../public/output/tossAndMatchWinForTeams.json';
const outHighestMotm= '../public/output/highestManOfMatchHolders.json';
const outBatsmanSR= '../public/output/batsmanStrikeRateThroughSeasons.json'; 
const outHighestDismissal= '../public/output/highestDismissalByBowler.json'; 
const outBestEconomyInSuperover= '../public/output/bestEconomyBowlerInSuperover.json'; 

function ipl(inputMatches, inputDeliveries) {

    const matchesObj = convertCsvToJson(inputMatches);
    const deliveriesObj = convertCsvToJson(inputDeliveries);

    const problem1 = calculateMatchesPerYear(matchesObj);
    const problem2 = calculateMatchesWonPerTeamPerYear(matchesObj);
    const problem3 = calculateTeamExtrasForYear(matchesObj, deliveriesObj, 2016);
    const problem4 = getTopTenEconomicalBowlerForYear(matchesObj, deliveriesObj, 2015);

    const extraProblem1= calculateTossAndMatchWinForTeams(matchesObj);
    const extraProblem2= getHighestManOfTheMatchPlayerPerSeason(matchesObj);
    const extraProblem3= calculateBatsmanStrikeRatePerSeason(matchesObj,deliveriesObj,'RG Sharma');
    const extraProblem4= getHighestTimesPlayerDismissal(deliveriesObj);
    const extraProblem5= getBestEconomyBowlerInSuperover(deliveriesObj);

    saveToFile(JSON.stringify(problem1), outMatchesPerYear);
    saveToFile(JSON.stringify(problem2), outMatchesWonByTeams);
    saveToFile(JSON.stringify(problem3), outTeamExtras);
    saveToFile(JSON.stringify(problem4), outTopBowlers);

    saveToFile(JSON.stringify(extraProblem1), outTossAndMatchWin);
    saveToFile(JSON.stringify(extraProblem2), outHighestMotm);
    saveToFile(JSON.stringify(extraProblem3), outBatsmanSR);
    saveToFile(JSON.stringify(extraProblem4), outHighestDismissal);
    saveToFile(JSON.stringify(extraProblem5), outBestEconomyInSuperover);

    return 'JSON updated successfully.'
}

module.exports=ipl;






// ...Modules testing below...

// const getBowlersStatPerMatch = require('./modules/getBowlersStatsPerMatch');
// const getAllBowlersStatsForYear = require('./modules/getAllBowlersStatsForYear');
// const getAllBowlersEconomyForYear = require('./modules/getAllBowlersEconomyForYear');
// const getExtrasPerMatchId = require('./modules/getExtrasPerMatchId');
// const getMatchIdOfYear = require('./modules/getMatchIdOfYear');

// console.log(getMatchIdOfYear(matchesObj,2016));
// console.log(getExtrasPerMatchId(deliveriesObj,595));
// console.log(getBowlersStatPerMatch(deliveriesObj,595));
// console.log(getAllBowlersStatsForYear(deliveriesObj,matchesObj,2016));
// console.log(getAllBowlersEconomyForYear(matchesObj,deliveriesObj,2016));
// console.log(calculateTossAndMatchWinForTeams(matchesObj));
// console.log(getManOfTheMatchCount(matchesObj));
// console.log(getHighestManOfTheMatchPlayerPerSeason(matchesObj));
// console.log(getBatsmanStatPerMatch(deliveriesObj, 100));
// console.log(getAllBatsmanStatsForYear(deliveriesObj,matchesObj,2015));
// console.log(calculateBatsmanStrikeRatePerSeason(matchesObj,deliveriesObj,'RG Sharma'));
// console.log(getBatsmanWicketTakers(deliveriesObj));
// console.log(getHighestTimesPlayerDismissal(deliveriesObj));
// console.log(getBowlerStatInSuperover(deliveriesObj));
// console.log(getAllBowlersStatsForYear(deliveriesObj,matchesObj,2016));
// console.log(getBestEconomyBowlerInSuperover(deliveriesObj));